% The Bloggers Club Charter
# The Bloggers Club

## What is it?
It's a loose grouping of people who blog and who want to blog. We decide a general topic to blog to each fortnight and then we all write to it. We can offer each other support, proof-reading, ideas, suggestions and constructive criticism.

## What is it not?
It's not a strict regime with a style guide and a well defined process (it's a very loosely defined process). There's no penalties for missing a deadline and there's no recriminations.

## So why do it?
If, like me, you: 

	* Struggle to think of ideas for things to blog about.
	* Can't get the motiviation to blog. 
	* Don't think your point of view is important enough to be heard.
	* Really want to get (back) into blogging.
	* Want a safe place to improve your writing.
	
Then you could well be interested in joining us! The goals are encouragement, motivation and improvement.

## What are the rules?
	* Be nice. 
	* Seriously, being nice is important.
	* Make an honest effort to blog. It's worthwhile showing your drafts if you want to improve, even if you don't get to actually publishing.
	* Every 2 weeks, we publish.
	* Every 2 weeks, offset from the publishing date, we decide the next topic. 
	* Criticising ideas and writing with constructive comments is fine. 
	* Personal attacks are not. 

## How do I join?
If you're in this channel, you've already joined, hurrah!